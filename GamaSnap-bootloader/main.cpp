#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <conio.h>
#include <inttypes.h>
#include "tserial.h"
//#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd

//#else
//#include <unistd.h>
//#define GetCurrentDir getcwd
//#endif

#define START_BYTE 0x7F
#define ACK_BYTE 0x79
#define NACK_BYTE 0x1F
#define WRITE_MEMORY_CMD 0x31
#define GO_CMD 0x21
#define BASE_ADDR 0x08000000
#define MAX_BLOCK_LENGTH 128

Tserial *_serialPort; //serial is a class type defined in these files, used for referring to the communication device

typedef union address_t {
	unsigned int integer;
	char byte[4];
} address;

bool acknowledge(uint8_t rxByte) {
	switch (rxByte)
	{
	case ACK_BYTE:
		return TRUE;
		break;
	case NACK_BYTE:
		return FALSE;
		break;
	default:
		return FALSE;
		break;
	}
}

void main() {

	//printf("Enter character to be sent"); //User prompt
	//scanf("%c", &data); //User input
	_serialPort = new Tserial();
	_serialPort->connect("COM4", 115200, serial_parity::spEVEN);

	uint8_t checksum_xor = 0;
	int index = 0;

	_serialPort->sendChar(START_BYTE);

	while (!acknowledge(_serialPort->getChar()));

	puts("Mass Erasing Memory . . .");

	uint8_t eraseCMD[] = { 0x44, 0xBB };

	_serialPort->sendArray((char *)eraseCMD, sizeof(eraseCMD));

	while (!acknowledge(_serialPort->getChar()));

	uint8_t fullEraseCMD[] = { 0xFF, 0xFF, 0x00 };

	_serialPort->sendArray((char *)fullEraseCMD, sizeof(fullEraseCMD));

	while (!acknowledge(_serialPort->getChar()));

	address_t addr;
	addr.integer = BASE_ADDR;

	

	uint8_t bytesRead = 0;
	FILE *fp;
	fp = fopen("sandbox.bin", "rb");
	if (fp == NULL) { fputs("File error", stderr); exit(1); }

	uint8_t buffer[MAX_BLOCK_LENGTH+2];
	buffer[0] = MAX_BLOCK_LENGTH - 1;
	int i = 0;
	while (bytesRead = fread(buffer + 1, sizeof(uint8_t), MAX_BLOCK_LENGTH, fp)) {

		
		uint8_t writeMemoryCMD[] = { WRITE_MEMORY_CMD, ~WRITE_MEMORY_CMD };

		_serialPort->sendArray((char *)writeMemoryCMD, sizeof(writeMemoryCMD));

		while (!acknowledge(_serialPort->getChar()));

		for (index = sizeof(BASE_ADDR) - 1, checksum_xor = 0; index >= 0; --index)
		{
			_serialPort->sendChar(addr.byte[index]);
			checksum_xor ^= addr.byte[index];
		}
		_serialPort->sendChar(checksum_xor);

		while (!acknowledge(_serialPort->getChar()));

		buffer[0] = bytesRead-1;

		for (index = 0, checksum_xor = 0; index <= bytesRead; index++)
			checksum_xor ^= buffer[index];
		buffer[bytesRead + 1] = checksum_xor;
		_serialPort->sendArray((char *)buffer, bytesRead+2);

		while (!acknowledge(_serialPort->getChar()));

		addr.integer += bytesRead;

		printf("%010d | %010d\n", ++i, bytesRead);
	}


	uint8_t goCMD[] = { GO_CMD, ~GO_CMD };
	_serialPort->sendArray((char *)goCMD, sizeof(goCMD));

	while (!acknowledge(_serialPort->getChar()));

	addr.integer = BASE_ADDR;

	for (index = sizeof(addr) - 1, checksum_xor = 0; index >= 0; --index)
	{
		_serialPort->sendChar(addr.byte[index]);
		checksum_xor ^= addr.byte[index];
	}
	_serialPort->sendChar(checksum_xor);

	while (!acknowledge(_serialPort->getChar()));

	_serialPort->disconnect();
	getchar();
}